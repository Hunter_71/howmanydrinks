FROM openjdk:jre-slim as prod

RUN apt-get -qq update --yes \
    && apt-get -qq install --yes --no-install-recommends \
        curl \
        ca-certificates \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /conf
COPY .editorconfig /conf

RUN mkdir /app
COPY . /app

WORKDIR /app

FROM prod as dev

# Download ktlint
ARG ktlint_version=0.35.0

RUN curl -sSLO https://github.com/pinterest/ktlint/releases/download/${ktlint_version}/ktlint \
    && chmod a+x ktlint \
    && mv ktlint /usr/local/bin \
    && true
