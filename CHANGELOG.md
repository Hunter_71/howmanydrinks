# CHANGELOG

## Unreleased

1) Add [**ktlint**](https://ktlint.github.io/) linter and formatter to project (#13)  
1) Add [**Docker**](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/) definition file (#13, #17)   
1) Add [**docker-compose**](https://docs.docker.com/compose/) file (#13)  
1) Add [README](README.md) file (#14)  

## Version 0.0.2 (2019-10-13)

1) Allow user to show list of entries for selected date (#4)  
1) Add CHANGELOG file (#9, #11)  
1) Small changes and different improvements for code quality (#7, #8)  

## Version 0.0.1 (2019-09-29)

1) Allow user to add entries for different dates (#1)  
1) Fix UI bug for cutting of top of the date on data picker field (#2)  
1) Small changes and different improvements for code quality  
