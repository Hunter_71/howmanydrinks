package pl.hunter71.howmanydrinks.utils

enum class Emoji(var code: String) {
    BEER_MUG("\uD83C\uDF7A"),
    SPORTS_MEDAL("\uD83C\uDFC5"),
    ;

    override fun toString(): String {
        return code
    }
}
