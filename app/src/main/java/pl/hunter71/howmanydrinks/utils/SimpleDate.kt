package pl.hunter71.howmanydrinks.utils

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class SimpleDate(year: Int, month: Int, day: Int) {
    val year: Int = year
    val month: Int = month
    val day: Int = day

    companion object {
        const val DEFAULT_DATE_FORMAT = "yyyy-MM-dd"
        const val DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss"

        fun getCurrentDate(format: String = DEFAULT_DATE_FORMAT): String =
            LocalDate.now().format(DateTimeFormatter.ofPattern(format))

        fun getCurrentDateTime(format: String = DEFAULT_DATETIME_FORMAT): String =
            LocalDateTime.now().format(DateTimeFormatter.ofPattern(format))

        fun todayTimestamp() = LocalDateConverter(LocalDate.now()).toTimestamp()

        fun toTimestamp(rawDate: String, format: String = DEFAULT_DATE_FORMAT): Int =
            parse(rawDate, format).toTimestamp()

        private fun parse(rawDate: String, format: String) =
            LocalDateConverter(LocalDate.parse(rawDate, DateTimeFormatter.ofPattern(format)))
    }

    private data class LocalDateConverter(val year: Int, val month: Int, val day: Int) {

        constructor(date: LocalDate) : this(date.year, date.monthValue, date.dayOfMonth)

        fun toTimestamp() = year * 10000 + month * 100 + day

        override fun toString(): String {
            return LocalDate.of(year, month, day).format(
                DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT)
            )
        }
    }
}
