package pl.hunter71.howmanydrinks.utils

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Calendar

class SimpleCalendar {
    private val calendar = Calendar.getInstance()

    val year: Int
        get() = calendar.get(Calendar.YEAR)

    val month: Int
        get() = calendar.get(Calendar.MONTH)

    val day: Int
        get() = calendar.get(Calendar.DAY_OF_MONTH)

    fun set(year: Int, month: Int, day: Int) {
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, day)
    }

    fun format(format: String): String {
        val monthValue = if (month < 12) month + 1 else 1
        return LocalDate.of(year, monthValue, day).format(DateTimeFormatter.ofPattern(format))
    }
}
