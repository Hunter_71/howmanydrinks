package pl.hunter71.howmanydrinks.db.contract

import android.provider.BaseColumns

object EntryDBEntry: BaseColumns, ITable {

    override val TABLE_NAME: String = "entries"

    object Column {
        val ID = "id"
        val DATE = "date"
        val TIME = "time"
        val QUANTITY = "quantity"
        val DESCRIPTION = "description"
        val ARCHIVED = "archived"
    }

    object DataType {
        val ID = "INTEGER PRIMARY KEY AUTOINCREMENT"
        val DATE = "INTEGER NOT NULL"
        val TIME = "INTEGER"
        val QUANTITY = "INTEGER NOT NULL"
        val DESCRIPTION = "TEXT NOT NULL"
        val ARCHIVED = "INTEGER NOT NULL"
    }

    override val CREATE_TABLE_SQL: String = (
        "CREATE TABLE $TABLE_NAME ("
            + "${Column.ID} ${DataType.ID}"
            + ", ${Column.DATE} ${DataType.DATE}"
            + ", ${Column.TIME} ${DataType.TIME}"
            + ", ${Column.QUANTITY} ${DataType.QUANTITY}"
            + ", ${Column.DESCRIPTION} ${DataType.DESCRIPTION}"
            + ", ${Column.ARCHIVED} ${DataType.ARCHIVED}"
            + ")"
        )

    override val DROP_TABLE_SQL: String = "DROP TABLE IF EXISTS $TABLE_NAME"
}
