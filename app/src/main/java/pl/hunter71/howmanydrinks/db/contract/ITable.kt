package pl.hunter71.howmanydrinks.db.contract

interface ITable {
    val TABLE_NAME: String

    val CREATE_TABLE_SQL: String
    val DROP_TABLE_SQL: String
}
