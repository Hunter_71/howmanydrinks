package pl.hunter71.howmanydrinks

import android.app.DatePickerDialog
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.button_add
import kotlinx.android.synthetic.main.activity_main.button_today
import kotlinx.android.synthetic.main.activity_main.editText_date
import kotlinx.android.synthetic.main.activity_main.editText_description
import kotlinx.android.synthetic.main.activity_main.editText_entriesDate
import kotlinx.android.synthetic.main.activity_main.progressBar_score
import kotlinx.android.synthetic.main.activity_main.spinner_volume
import kotlinx.android.synthetic.main.activity_main.textView_list
import kotlinx.android.synthetic.main.activity_main.textView_progress
import kotlinx.android.synthetic.main.activity_main.textView_target
import kotlinx.android.synthetic.main.activity_main.textView_title
import kotlinx.android.synthetic.main.activity_main.textView_volumes
import pl.hunter71.howmanydrinks.db.DBHelper
import pl.hunter71.howmanydrinks.db.model.Entry
import pl.hunter71.howmanydrinks.utils.Emoji
import pl.hunter71.howmanydrinks.utils.LoggingTags
import pl.hunter71.howmanydrinks.utils.SimpleCalendar
import pl.hunter71.howmanydrinks.utils.SimpleDate

class MainActivity: AppCompatActivity() {

    val ACTION_HIDE = "hide"
    val ACTION_SHOW = "show"

    val DATE_FORMAT = "dd-MM-yyyy"
    val TARGET_VOLUME: Double = 2.0

    private var calendar = SimpleCalendar()

    private lateinit var textMessage: TextView
    private val onNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    textMessage.setText(R.string.title_home)

                    initEntriesListView()

                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_dashboard -> {
                    textMessage.setText(R.string.title_dashboard)

                    initAddEntryView()

                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_notifications -> {
                    textMessage.setText(R.string.title_notifications)

                    controlsHome(ACTION_HIDE)
                    controlsDashboard(ACTION_HIDE)

                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        textMessage = findViewById(R.id.message)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        backup()
        initEntriesListView()
    }

    override fun onResume() {
        super.onResume()

        initEntriesListView()
    }

    private fun initAddEntryView() {
        refreshInputs()

        controlsHome(ACTION_HIDE)
        controlsDashboard(ACTION_SHOW)
    }

    private fun initEntriesListView() {
        fun getTargetText(): String {
            return "Cel:  $TARGET_VOLUME l ${Emoji.BEER_MUG}"
        }

        fun setTodayOnButtonClick() {
            button_today.setOnClickListener {
                setCurrentDate(editText_entriesDate)
            }
        }

        textView_target.text = getTargetText()

        setTodayOnButtonClick()

        reloadEntriesOnDateChange(editText_entriesDate)
        setCurrentDate(editText_entriesDate)
        showDataPickerOnClick(editText_entriesDate)

        controlsDashboard(ACTION_HIDE)
        controlsHome(ACTION_SHOW)
    }

    private fun calcVolume(entries: List<Entry>): Double {
        return entries.sumBy { entry -> entry.quantity } / 1000.0
    }

    private fun getEntries(date: String): List<Entry> {
        return DBHelper(this).selectEntriesByDate(SimpleDate.todayTimestamp())
    }

    private fun refreshEntries(entries: List<Entry>) {
        fun getEntriesDescriptionsText(): String {
            return entries.map { x -> x.description }.joinToString("\n")
        }

        fun getEntriesVolumesText(): String {
            fun adjustUnit(quantity: Int): String {
                if (quantity < 500) {
                    return "$quantity ml"
                }

                return "${quantity / 1000.0} l"
            }

            return entries.map { x -> adjustUnit(x.quantity) }.joinToString("\n")
        }

        textView_list.text = getEntriesDescriptionsText()
        textView_volumes.text = getEntriesVolumesText()

        textView_list.movementMethod = ScrollingMovementMethod()
    }

    private fun refreshProgress(volume: Double) {
        fun calcProgressPercentage(volume: Double): Int {
            return (volume / TARGET_VOLUME * 100).toInt()
        }

        fun isTargetAchieved(volume: Double): Boolean {
            return volume >= TARGET_VOLUME
        }

        fun setProgressText(volume: Double, progress: Int) {
            var progressText = "RAZEM: $volume l   ($progress%)"

            if (isTargetAchieved(volume)) {
                progressText += " ${Emoji.SPORTS_MEDAL}"
            }

            textView_progress.text = progressText
        }

        fun setProgressBar(volume: Double, progress: Int) {
            val progressBarColor = ColorStateList.valueOf(
                if (isTargetAchieved(volume)) Color.GREEN else Color.RED
            )

            progressBar_score.progress = progress
            progressBar_score.setProgressTintList(progressBarColor)
        }

        val progress = calcProgressPercentage(volume)
        setProgressText(volume, progress)
        setProgressBar(volume, progress)

        Log.d(LoggingTags.MainActivity_OnCreate.tag, "Progress: $progress%")
    }

    private fun refreshInputs() {
        fun getVolumesList(): List<Int> {
            return resources.getStringArray(R.array.volumes).map { x -> x.toInt() }
        }

        fun addEntryOnButtonClick() {
            val spinnerVolumeItems: List<Int> = getVolumesList()

            button_add.setOnClickListener {
                if (
                    editText_date.text.isNotBlank()
                    && editText_description.text.isNotBlank()
                    && spinner_volume.selectedItemPosition > 0
                ) {
                    val dateText: String = editText_date.text.toString()
                    val date: Int = SimpleDate.toTimestamp(dateText, DATE_FORMAT)
                    val description: String = editText_description.text.toString()
                    val quantity: Int = spinnerVolumeItems[spinner_volume.selectedItemPosition]

                    DBHelper(this).insert(Entry(description, quantity, date))

                    afterAddEntryCleanup()
                }
            }
        }

        textView_title.text = "Właśnie wypiłam:"

        setCurrentDate(editText_date)
        showDataPickerOnClick(editText_date)
        addEntryOnButtonClick()
    }

    private fun afterAddEntryCleanup() {
        editText_description.setText("")
        spinner_volume.setSelection(0)
    }

    private fun controlsHome(action: String) {
        val controls = listOf<View>(
            textView_target,
            editText_entriesDate,
            button_today,
            textView_list,
            textView_volumes,
            textView_progress,
            progressBar_score
        )
        when (action) {
            ACTION_SHOW -> show(controls)
            ACTION_HIDE -> hide(controls)
        }
    }

    private fun controlsDashboard(action: String) {
        val controls = listOf<View>(
            textView_title, editText_date, editText_description, spinner_volume, button_add
        )
        when (action) {
            ACTION_SHOW -> show(controls)
            ACTION_HIDE -> hide(controls)
        }
    }

    private fun setCurrentDate(dateField: EditText) {
        dateField.setText(SimpleDate.getCurrentDate(DATE_FORMAT))
        calendar = SimpleCalendar()
    }

    private fun showDataPickerOnClick(dateField: EditText) {
        dateField.setOnClickListener {
            val datePicker = DatePickerDialog(this,
                DatePickerDialog.OnDateSetListener { _, year, month, day ->
                    calendar.set(year, month, day)
                    dateField.setText(calendar.format(DATE_FORMAT))
                },
                calendar.year,
                calendar.month,
                calendar.day
            )
            datePicker.show()
        }
    }

    private fun reloadEntriesOnDateChange(dateField: EditText) {
        dateField.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable) {
                val date = s.toString()
                val entries = getEntries(date)
                val volume = calcVolume(entries)

                refreshEntries(entries)
                refreshProgress(volume)
            }
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) { }
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) { }
        })
    }

    private fun show(controls: List<View>) {
        controls.forEach { x -> x.visibility = View.VISIBLE }
    }

    private fun hide(controls: List<View>) {
        controls.forEach { x -> x.visibility = View.INVISIBLE }
    }

    private fun backup() {
        DBHelper(this).backup()
    }
}
