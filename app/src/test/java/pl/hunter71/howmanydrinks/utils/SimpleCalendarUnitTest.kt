package pl.hunter71.howmanydrinks.utils

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import org.junit.Assert
import org.junit.Test

class SimpleCalendarUnitTest {
    @Test
    fun init() {
        val format = "YYYY-MM-dd"
        val today = LocalDate.now().format(DateTimeFormatter.ofPattern(format))

        Assert.assertEquals(today, SimpleCalendar().format(format))
    }

    @Test
    fun set() {
        val expectedYear = 2000
        val expectedMonth = 10
        val expectedDay = 5

        val calendar = SimpleCalendar()
        calendar.set(expectedYear, expectedMonth, expectedDay)

        Assert.assertEquals(expectedYear, calendar.year)
        Assert.assertEquals(expectedMonth, calendar.month)
        Assert.assertEquals(expectedDay, calendar.day)
    }

    @Test
    fun format() {
        val calendar = SimpleCalendar()
        calendar.set(2000, 9, 5)

        Assert.assertEquals("2000-10-05", calendar.format("YYYY-MM-dd"))
    }
}
