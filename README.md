# How Many Drinks

Register daily drinks consumption  

## Dedication

To my beloved wife **Aleksandra**.  

It's for her.

## Details

Android application write in [Kotlin](https://kotlinlang.org/)  

## Release notes

### Latest release

Latest **How Many Drinks** version [**<0.0.2>**](https://gitlab.com/Hunter_71/howmanydrinks/-/tags/0.0.2) released in 2019-10-13

### Changelog

Versions change list is available with [**CHANGELOG**](CHANGELOG.md) file

## Codestyle checking & formatting

Run docker container with [dockerrun](dockerrun) script from root directory and then run one of scripts from **bin** directory:
```bash
$ ./dockerrun
root@container:/app# ./bin/codestyle
root@container:/app# ./bin/codeformat
```  

## Authors

### Originator

Aleksandra Panek-Zięba

### Leading developer

**Tomasz Zięba** [Hunter_71](https://gitlab.com/Hunter_71)   

### Contributors

**Marcin Józefowicz** [mjozefowicz](https://gitlab.com/mjozefowicz)  
